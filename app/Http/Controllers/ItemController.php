<?php

namespace App\Http\Controllers;

use App\Item;
use App\User;
use Illuminate\Http\Request;
use App\Services\ItemService;
use App\Http\Requests\EditItemRequest;
use App\Http\Requests\StoreItemRequest;

class ItemController extends Controller
{
    private $items;

    public function __construct(ItemService $itemService)
    {
        $this->items = $itemService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('items.index')->withItems($this->items->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all() ;
        return view("items.create")->withUsers($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreItemRequest $request)
    {
        $this->items->create($request->all(['content','user_id']));
        return redirect()->route('items.index')->with('flash', [
            'type' => 'success',
            'message' => 'Item créé avec succès !'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $users = User::all() ;
        return view("items.update")->withItem($item)->withUsers($users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(EditItemRequest $request, Item $item)
    {
        $this->items->update($item, $request->all(['content', 'user_id']));
        return redirect()->route('items.index')->with('flash', [
            'type' => 'success',
            'message' => 'Item modifié avec succès !'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        if ($this->items->delete($item))
            return redirect()->route('items.index')->with('flash', [
            'type' => 'success',
            'message' => 'Item supprimé avec succès !'
        ]);
        else
            return redirect()->route('items.index')->with('flash', [
            'type' => 'danger',
            'message' => 'Une erreur est survenue'
        ]);
    }
}
