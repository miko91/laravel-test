<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class MarieController extends Controller
{
    public function test_function(){
        $tableau = [
        ["nom" => "nico"],
        ["nom" => "marie"],
        ["nom" => "mikael"],
        ["nom" => "yousra"],
        ] ;

        $name="coucou";
//        return view('marie')->withName($name);
        return view('marie', ['name' => $tableau, 'status'=> "nul", 'items'=> Item::all()]);
        // return view('marie')->with('name', $name);
    }
}
