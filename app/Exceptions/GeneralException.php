<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class GeneralException extends Exception
{
    /**
     * @var $title string
     */
    private $title;

    /**
     * @var $type string
     */
    private $type;

    public function __construct($title, $message, Throwable $previous = null, $type = 'message')
    {
        parent::__construct($message, 0, $previous);
        $this->title = $title;
        $this->type = $type;
    }

    /**
     * Get GeneralException title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get GeneralException type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function getTrim()
    {
        return trim(preg_replace('/\s+/', ' ', $this->getMessage()));
    }
}

