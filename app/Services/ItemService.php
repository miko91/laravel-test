<?php

namespace App\Services;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\DB;

use App\Item;

class ItemService
{
    public function all()
    {
        $items = DB::table('items')
            ->select('items.*', 'users.name', 'users.email')
            ->leftJoin('users', 'users.id', '=', 'items.user_id')
            ->get();
        return $items ;
    }

    public function update(Item $item, $data)
    {
        //throw new GeneralException('Une erreur est survenue', 'Juste comme ça', null);
        $item->content = @$data['content'] ? $data['content'] : $item->content ;
        $item->user_id = $data['user_id'] ;
        if ($item->isDirty())
        {
            $item->save();
        }
    }

    public function create($data)
    {
        $item =  new Item($data);
        return $item->save();
    }

    public function delete(Item $item)
    {
        try {
            throw new \Exception('Fait exprès');
            return $item->delete();
        } catch (\Exception $e) {
            throw new GeneralException('Une erreur est survenue', 'On ne sait pas pourquoi', $e);
        }
    }
}
