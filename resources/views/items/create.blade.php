@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default col-lg-8 col-lg-offset-2">
            <div class="panel-header">
                <h2>Créer un nouvel item</h2>
                <hr>
            </div>
            <div class="panel-body">
                <form class="horizontal-form" method="POST" action="{{ route('items.store') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('content') ? ' has-error' : ''}}">
                    <label for="content" class="control-label col-sm-2">{{ ucfirst(__('validation.attributes')['content']) }}</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" id="content" name="content" autofocus value="{{ old('content') }}">
                    @if($errors->has('content'))
                        <span class="help-block">{{ $errors->first('content') }}</span>
                    @endif
                    </div>
                    <br><br>
                    <label for ="user" class="control-label col-sm-2">Utilisateur</label>
                    <div class="col-sm-10">
                        <select class="form-control" type="text" id="user_id" name="user_id" autofocus value="{{ old('user_id') }}">
                                <option></option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                    @if($errors->has('content'))
                        <span class="help-block">{{ $errors->first('content') }}</span>
                    @endif
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Créer</button>
                </form>
            </div>
        </div>
    </div>
</div>



@stop
