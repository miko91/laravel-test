@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default col-lg-8 col-lg-offset-2">
            
            <div class="panel-header">
                <h2>
                    MES ITEMS
                    <a class="btn btn-primary btn-xs pull-right" href="{{ route('items.create') }}">
                        <i class="fa fa-plus"></i>
                        Créer
                    </a>
                </h2>
                <hr>
            </div>
            <div class="panel-body">
                @if(session('flash'))
                <div class="alert alert-{{ session('flash')['type'] }}">
                    <p>{{ session('flash')['message'] }}</p>
                </div>
                @endif
                <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Assigné à</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($items as $item)
                    <tr>
                        <td>{{ $item->content }}</td>
                        <td>{{ $item->name }}</td>
                        <td>
                            
                            <form method="POST" action="{{ route('items.destroy', ['item' => $item->id ]) }}">
                            <a class="btn btn-primary btn-xs" href="{{ route('items.edit', ['item' => $item->id ]) }}">
                                <i class="fa fa-edit"></i>
                                Modifier
                            </a>
                                {{ csrf_field() }}
                                {{ method_field("DELETE") }}
                                <button type="submit" class="btn btn-danger btn-xs">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



@stop
