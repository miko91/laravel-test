@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default col-lg-8 col-lg-offset-2">
            
            <div class="panel-header">
                <h2>
                    UTILISATEURS
                    <a class="btn btn-primary btn-xs pull-right" href="{{ route('users.create') }}">
                        <i class="fa fa-plus"></i>
                        Créer
                    </a>
                </h2>
                <hr>
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            
                            <form method="POST" action="{{ route('users.destroy', ['user' => $user->id ]) }}">
                                <a class="btn btn-success btn-xs" href="{{ route('users.todo', ['user' => $user->id ]) }}">
                                    <i class="fa fa-edit"></i>
                                    Voir la TODO
                                </a>
                                <a class="btn btn-primary btn-xs" href="{{ route('users.edit', ['user' => $user->id ]) }}">
                                    <i class="fa fa-edit"></i>
                                    Modifier
                                </a>
                                {{ csrf_field() }}
                                {{ method_field("DELETE") }}
                                <button type="submit" class="btn btn-danger btn-xs">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>




@stop
